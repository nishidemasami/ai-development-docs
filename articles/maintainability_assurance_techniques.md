# 保守性担保手法

AIによる開発を進める中で、保守性が問題になります。  
開発・運用・保守を継続してゆく中で、しばしば人の手で保守性を担保しなければならないケースもあります。  

この章では、保守性を担保する手法を紹介します。

## 保守可能とは？

まず、保守性が最悪なシステムを考えてみましょう。

プログラミング言語はPythonやPHP、フロントエンドはJavaScriptで書かれていて、コメントもテストコードも書かれていません。  
設計書はエクセルやワードで書かれていて、いろんな人がコッソリ修正してばかりのバージョンがいくつもあり、いつ誰が何を修正したのか、どれが正しいのか、どれが最新なのか、さっぱりわかりません。エクセルやワードではGitで差分を管理することは難しく、特に図の比較はほぼ不可能に近いです。  
クラス図は書かれておらず、プログラムを見てみると行き当たりばったりで作ったようなクラスがでたらめに散らばっていて、なにかを修正しようにも修正が必要な箇所があちこちに散らばっていて把握が困難で、どこかを修正するとどこかでバグが発生したりします。  
自動テストも書かれておらず、OSやライブラリがバージョンアップした時にシステムに影響がないかどうかを確認できる人は誰もいません。  
開発環境や本番環境は、まるで秘伝のタレのように継ぎ足しに継ぎ足しを重ねて構築されていて、なにが設定されてなにがインストールされているのか全貌を把握している人は誰もいません。  
EC2の仮想マシン上にRuby on RailsやLaravel、Django、などが配置されていて、DBも認証システムもファイルも、システムのすべてがEC2の中にある場合、可用性もありませんしスケールもしません。テストも難しく、そしてEC2が落ちた時にはできることはなにもありません。  

次に、保守性が最高のシステムを考えてみましょう。

プログラミング言語はRustやKotlin、フロントエンドはTypeScriptで書かれていて、ソースにはコメントが充実しています。  
自動テストが書かれていて、もしAIが誤った内容のプログラムの修正をコミットしてしまった場合には即座に自動テストが失敗しアラートを通知します。  
ドキュメントはMarkdownで書かれGitで管理されていて、すべての編集において内容と編集者と日付が記録されており、図の矢印の方向一つすら記録から逃れることはありません。  
設計書にはクラス図が書かれており、その設計はSOLID原則に従ったものであるため修正時にも秩序が保たれます。  
自動テストがちゃんと書かれていて、OSやライブラリがバージョンアップした時でも品質を担保して柔軟にアップデートすることが可能です。  
開発環境は誰もが開発に参加しやすいようにGitHub Codespacesなどでいつでも起動可能な状態になっています。  
本番環境はTerraformやCloudFormationで管理され、すべての変更内容は保存されていていつでも確認でき再現可能になっています。  
処理はLambda、データベースはDynamoDB、ファイル保存はS3、配信はCloudFront、認証はCognitoを使用していて、可用性がありスケールし各機能は独立していてテスト可能です。  

この章では、システムの保守性担保手法を説明します。  
保守性の高いシステム設計は、なにもAIによる開発にだけ役に立つものではありません。例えば、新人によるOJT開発やオフショア開発、下請けへの発注でも有効です。バグが少なく保守性の高いシステム開発はコントロール可能であることを説明します。

## プログラミング言語

PythonやPHP、RubyやJavaScriptで開発したシステムは、型の安全性に乏しく、また静的解析も難しいためそのコードが実行されるまでバグの発見が難しい場合があり、その結果、本番で突然エラーが発生するなどの憂き目に遭うのです。  
確かにこれらの言語は開発するのがとても簡単なため開発速度が速いように見え、しかもその簡単さから要員も確保しやすいいですが、とても保守性があるとは言い難いです。  

対して、RustやKotlin、TypeScriptは一見開発速度は遅いように思えますがそれはバグの顕在化のタイミングがテストフェースや運用フェーズから開発フェーズに移動しただけであって、テストや運用時に発覚したバグの修正にはさらに手間もかかりますし、そういったコストが軽減されることを鑑みると全体的な開発工数はむしろこれらの言語の方が速いと言えるでしょう。また、要員の確保も一見難があるように感じられるものの、AIによるシステム開発が可能になった現代では要員確保の難しさはそれほど問題にはなりません。  

## 保守のしやすいクラス設計

保守性を一切考えていないプログラムでは、1つの修正でシステム全体の修正が必要になったりします。  
それに対して保守性を考慮されて設計されたプログラムでは、1つの修正の影響範囲はその修正箇所に限定されていてバグの発生も抑えることが可能です。

システムが保守可能なように設計するために「SOLID原則」という手法があります。

### SOLID原則

SOLID原則とは、守ることで保守が容易なシステムを開発することができる5つの原則です。  
その5つとは以下の通りです。  

- 単一責任の原則 (single-responsibility principle)
- 開放閉鎖の原則（open/closed principle）
- リスコフの置換原則（Liskov substitution principle）
- インターフェース分離の原則 (interface segregation principle)
- 依存性逆転の原則（dependency inversion principle）

一つ一つは一見難しそうに見えますが、一つずつかみ砕いて読んでいけば現代のプログラマにとっては当たり前のことばかり書いてあります。  

以下のクラス図を見てください。

```plantuml
@startuml

interface Iterator<Item> {
  -type Item
  +next(&mut self) -> Option<Self::Item>
}

note top of Iterator
  <b>■Iteratorトレイト</b>
  イテレータを実装するためのトレイト。
  先頭から順次走査可能な
  オブジェクトであることを示す。
end note

struct Vec <Item> {
  -buf: RawVec<Item>
  -len: usize
  +count(self) -> usize
  +nth(&mut self, n: usize) -> Option<Self::Item>
  +len(&self) -> usize
  +next(&mut self) -> Option<Self::Item>
}

note bottom of Vec 
  <b>■Vec構造体</b>
  サイズを変更可能な配列。
  そのサイズはコンパイル時には不定で、
  （配列のサイズが可変なのは他言語では普通だが
  Rustでは安全性のため配列の要素数は不変である）
  いつでも要素の追加・削除が可能。
  内部では配列で値を保持している。
end note

struct LinkedList <Item> {
  -head: Option<NonNull<Node<Item>>>
  -tail: Option<NonNull<Node<Item>>>
  -len: usize
  -alloc: Allocator
  +next(&mut self) -> Option<Self::Item>
}

note bottom of LinkedList
  <b>■LinkedList構造</b>
  Vecと同様にサイズを変更可能な配列。
  Vecは配列で実装されているが
  LinkedListは連結リストで実装されている。
end note

Iterator <|-- Vec
Iterator <|-- LinkedList

@enduml
```

これはRustの、Iteratorトレイトと、Iteratorを配列で実装しているVec構造体、Iteratorを連結リストで実装しているLinkedList構造体のクラス図です。これを参照しながら説明してゆきます。

- 単一責任の原則 (single-responsibility principle)

「モジュール、クラスまたは関数が提供するサービスは、その責任と一致している必要がある。」という原則です。  
例えばVec構造体はその名前の通り「ベクタ型」としてのふるまいを実装しているだけに限定されています。このベクタ型のふるまいを修正する場合でも、IteratorトレイトにもLinkedList構造体にも影響はありません。なぜならば、Iteratorトレイトはベクタ型としての責任は持っておらず、Iteratorのふるまいに修正が発生する時だけしかIteratorトレイトには修正が発生しないからです。  

```plantuml
@startuml

interface Iterator<Item> {
  -type Item
  +next(&mut self) -> Option<Self::Item>
}

note top of Iterator
  イテレータのふるまいの仕様が変更される時にしか
  Iteratorトレイトには修正が発生しない。
end note

struct Vec <Item> {
  -buf: RawVec<Item>
  -len: usize
  +count(self) -> usize
  +nth(&mut self, n: usize) -> Option<Self::Item>
  +len(&self) -> usize
  +next(&mut self) -> Option<Self::Item>
}

struct LinkedList <Item> {
  -head: Option<NonNull<Node<Item>>>
  -tail: Option<NonNull<Node<Item>>>
  -len: usize
  -alloc: Allocator
  +next(&mut self) -> Option<Self::Item>
}

Iterator <|-- Vec
Iterator <|-- LinkedList

note "Vec構造体はその名前の通り「ベクタ型」としての\nふるまいを実装しているだけに限定されているため\nベクタ型のふるまいを修正する場合でも\nIteratorトレイトにもLinkedList構造体にも影響はない" as Note
Vec .. Note

@enduml
```
これが単一責任の原則が保たれている状態です。

- 開放閉鎖の原則（open/closed principle）

「ソフトウェア要素（クラス、モジュール、関数など）は、拡張に対しては開いており、修正に対しては閉じているべきである。」という原則です。  
例えばVec構造体のnextメソッドを修正する場合でも、IteratorトレイトにもLinkedList構造体にも影響はありません。また、Vec構造体にはcountメソッドやnthメソッドなどいろいろ追加されていますがこれらを修正する場合でも、IteratorトレイトにもLinkedList構造体にも、そしてそれらを使用しているプログラムにも影響はありません。  

```plantuml
@startuml

interface Iterator<Item> {
  -type Item
  +next(&mut self) -> Option<Self::Item>
}

struct Vec <Item> {
  -buf: RawVec<Item>
  -len: usize
  +count(self) -> usize
  +nth(&mut self, n: usize) -> Option<Self::Item>
  +len(&self) -> usize
  +next(&mut self) -> Option<Self::Item>
}

struct LinkedList <Item> {
  -head: Option<NonNull<Node<Item>>>
  -tail: Option<NonNull<Node<Item>>>
  -len: usize
  -alloc: Allocator
  +next(&mut self) -> Option<Self::Item>
}

Iterator <|-- Vec
Iterator <|-- LinkedList

note "Vecの修正や機能追加が\nIteratorトレイトにもLinkedList構造体にも\nそしてそれらを使用しているプログラムにも\n影響を与えることはない" as Note
Vec .. Note

@enduml
```

これが開放閉鎖の原則が保たれている状態です。

- リスコフの置換原則（Liskov substitution principle）

「オブジェクト指向プログラミングにおいて、サブタイプのオブジェクトはスーパータイプのオブジェクトの仕様に従わなければならない。」という原則です。  
例えばIteratorトレイトを実装した値を引数に取る関数の場合、その実体がVecで実装されたものでもLinkedListで実装されたものでも、動作は変わりません。  
これがリスコフの置換原則が保たれている状態です。

- インターフェース分離の原則 (interface segregation principle)

「インターフェースの利用者にとって不要なメソッドへの依存を強制してはいけない」という原則です。  
例えばIteratorトレイトは`next(&mut self) -> Option<Self::Item>`というメソッドを定義していますが、もし`nth(&mut self, n: usize) -> Option<Self::Item>`まで定義してしまったら、n番目の要素を取得するのは連結リストではコストが多いためLinkedListでIteratorトレイトの実装が難しいという事態になってしまいます。  
そのようなことにならないように、Iteratorトレイトはその名前が示す通り`Iterator`として最低限のメソッドだけが定義されています。  
これがインターフェース分離の原則が保たれている状態です。  

- 依存性逆転の原則（dependency inversion principle）

「上位モジュールはいかなるものも下位モジュールから持ち込んではならない。双方とも抽象に依存するべきである。」「抽象は詳細に依存してはならない。詳細（具象的な実装内容）が抽象に依存するべきである。」という原則です。  
例えばIteratorトレイトがVecの実装に引っ張られて`nth`や`count`、`len`を実装した場合、IteratorトレイトがVecの私物化されてしまっていてIteratorトレイトの役割を果たさず、Iteratorトレイトを実装しているすべてのコードに不要な影響(LinkedListなのに`nth`を実装しなければならない！など)が発生してしまうことになります。  
そのようなことにならないように、Iteratorトレイトはいかなる実装にも依存していない状態が保たれています。  
これが依存性逆転の原則が保たれている状態です。  

以上の5つをきちんと守れば、保守性が担保されているシステムが開発可能です。

## IaC(Infrastructure as Code)、およびマイクロサービスアーキテクチャ・サーバレスアーキテクチャ

開発環境や本番環境が、まるで秘伝のタレのように継ぎ足しに継ぎ足しを重ねて構築されていて、どうなっているのか誰も把握していないということはありませんか？  
そういうことは世界中で発生していて、そしてこんな困った事態が発生することを防ぐために「IaC(Infrastructure as Code)」が考案されました。  

IaCとはコードでインフラ構成を定義することで、インフラが属人化することを防ぎ、運用・保守や開発環境の構築、そしてケアレスミスなどの発生を防いだり、履歴が厳密に管理されることでセキュリティ上の問題が発生することを防ぐことが可能です。

IaCには以下のようなライブラリが利用されています。

- Docker
- Terraform・CloudFormation

ここでは、IaCで使用するライブラリを紹介します。

### Docker

Dockerは今やソフトウェアの開発や運用に絶対に欠かすことのできないものです。  
このドキュメントをHTMLやPDFに変換するのにもDockerを使用しています。  

サンプルとして、本ドキュメントをHTMLやPDFに変換に使用しているDockerfileを説明します。
```docker
FROM debian:12-slim

RUN apt update && \
	apt install calibre openjdk-17-jre-headless nodejs npm graphviz fonts-noto-cjk -y && \
	groupadd -g 1000 honkit && useradd -m -d /home/honkit -s /bin/bash -u 1000 -g 1000 honkit

USER honkit
RUN mkdir ~/.npm-global && \
	npm config set prefix '~/.npm-global' && \
	echo export PATH=~/.npm-global/bin:$PATH >> ~/.profile && \
	. ~/.profile && \
	npm install -g honkit gitbook-plugin-uml gitbook-plugin-hide-published-with 

WORKDIR /honkit
```
見ればなんとなく理解できると思いますが、Debianを土台として必要なパッケージをインストールしてから環境設定をしています。  
このDockerfileを僕は以下のリポジトリで公開しています。  
<https://github.com/nishidemasami/markdown-docs-dockerfile>  
これにより誰でもどんな環境でもDocker上で同じことを実行できるようになっています。  
実際にこれは、Windowsデスクトップ上でも、Linux上でも、そしてこのウェブサイトをビルドをしているGitLab CI/CDでも全く同じように動作しています。

### Terraform・CloudFormation

Dockerは仮想マシン単位のIaCでしたが、Terraformはさらにそれを拡張して、AWSやGoogle Cloudなどクラウドの複数サービスからなるインフラ構成をすべて一括管理できるようになっています。  
Google Duet AIはTerraformによりクラウドのインフラを操作します。  
CloudFormationはAWSに特化したIaCで、AWSしか使わない場合にはTerraformよりもCloudFormationの方が便利なケースもあります。  

TerraformやCloudFormationなどのこれらIaCは現代のクラウド時代で保守可能な継続的なシステム開発をする上で欠かすことのできない存在です。

### マイクロサービスアーキテクチャ・サーバレスアーキテクチャ

一つの巨大なシステムを保守することは非常に難しいですが、小さな単位で互いに完全に独立しているシステムを個々別途保守することは非常に容易です。

例として、以下のようなアーキテクチャは保守を考慮する上で都合が良いように構成されています。  
- マイクロサービスアーキテクチャ  
  システムを構成する各要素を「マイクロサービス」と呼ばれる独立した小さなコンポーネントとして実装するアーキテクチャです。
- サーバレスアーキテクチャ  
  常時稼働するサーバは構築・運用せず、必要に応じて必要な量のリソースだけをクラウド上で随時確保するアーキテクチャです。

AWSを利用したシステムはマイクロサービスアーキテクチャおよびサーバレスアーキテクチャを活用していると言えます。  
たとえば、認証にCognito、APIにAPI Gateway、処理にLambda、データの保存にDynamoDB、CDNにCloudFront、静的ファイルの格納にS3、とAWSの各サービスを使用している場合、デバッグも容易で、運用も手作業が発生せずミスも無く、またアクセスに応じてオートスケールが自動で行われるため無駄なコストも発生せず、そしてAIによるインフラまで含めた包括的な開発が可能です。  

例として以下のようなシステムを考えます。

```plantuml
@startuml

' https://www.plantuml.com/plantuml/uml/

skinparam shadowing false
skinparam linetype ortho
skinparam rectangle {
    BackgroundColor AWS_BG_COLOR
    BorderColor transparent
}

title AWSを活用したマイクロサービスアーキテクチャ・サーバレスアーキテクチャのサンプル
top to bottom direction

!include <logos/github-icon>
!include <logos/github-actions>
!include <logos/branch-icon>
!include <awslib/AWSCommon.puml>
!include <awslib/General/Documents.puml>
!include <awslib/General/Users.puml>
!include <awslib/Groups/Generic.puml>
!include <awslib/SecurityIdentityCompliance/Cognito.puml>
!include <awslib/Storage/SimpleStorageService.puml>
!include <awslib/NetworkingContentDelivery/CloudFront.puml>
!include <awslib/General/Client.puml>
!include <awslib/GroupIcons/Cloudalt.puml>
!include <awslib/ApplicationIntegration/APIGateway.puml>
!include <awslib/Compute/Lambda.puml>
!include <awslib/Database/DynamoDB.puml>
!include <awslib/AWSSimplified.puml>


Users(clientUser, "ユーザー", User client device)

Cloudalt(aws, "AWS Cloud", AWS) {
  CloudFront(cloudFront, "CloudFront\nCDNとして使用", "CloudFront")
  SimpleStorageService(simpleStorageService, "S3\n静的ファイルを格納", "S3")
  Cognito(cognito, "Cognito\n認証に使用", "Cognito")
  APIGateway(apiGateway, "API Gateway\n認証の検証などに使用", "API Gateway")
  Lambda(lambda, "Lambda\nデータ処理に使用", "Lambda")
  DynamoDB(dynamodb, "DynamoDB\nデータ保存に使用", "DynamoDB")
  GenericGroup(components5,バッチ処理) #Transparent {
    Lambda(lambda2, "Lambda\n登録されたデータを元にファイルを生成", "Lambda")
  }
}

clientUser 0-d-> cloudFront: ファイルの要求
cloudFront 0-d-> simpleStorageService: ファイルの要求
apiGateway 0-d-> lambda: 処理実行
lambda 0-d-> dynamodb: データ登録
cloudFront -[hidden]r- cognito
clientUser 0-d-> cognito: 認証
apiGateway -l-> cognito: 認証情報\n取得
clientUser 0-d-> apiGateway: リクエスト送信\n（認証付き）

dynamodb -l-> lambda2: "データを取得"
lambda2 -u-> simpleStorageService: "ファイルを生成・変更"

@enduml
```

これらの、CloudFrontやDynamoDBといったサービス一つ一つはメンテナンスが容易であり、利用者数に応じて自動でスケールします。極端な話一切利用がなければ0円です。  
この「マイクロサービスアーキテクチャ」は、小さなサービスから始めて徐々に規模を拡大しながら機能を拡張しつつ運用・保守をするアジャイル開発がしやすいアーキテクチャです。  
また「サーバレスアーキテクチャ」としてEC2などのいわゆる「サーバ」を使用していません。    
というのも、EC2には以下のような様々な欠点があるためです。

- 自動でスケールしない  
  利用者が少なかったり、あるいは利用が一切無かったとしても決められた費用が請求されてしまい、また利用者が増えてもその分処理が遅くなるだけで自動でサーバを増強などはしてくれません。
- SLAが低い  
  サーバはクラウド事業者としても保守性が悪く、システムダウンが多いです。
- IaC化が難しい  
  サーバが秘伝のタレ化してしまいやすいです。
- 運用が難しい  
  サーバのセキュリティアップデートなどでシステム停止が発生したりします。

これらの欠点を嫌い、これらの欠点を全て克服した「サーバレスアーキテクチャ」が保守性を求める新しいシステム開発では主流となってきているのです。


## ドキュメント保守

ドキュメントは関係者間の意思疎通のための共通言語です。  
ドキュメントがスムーズに連携され、保守され、そして管理されなければ、関係者の意思疎通は不可能です。

一般的にエクセルやワードで書かれたドキュメントは管理が難しく、充分多くの時間を費やせば可能ではあるかもしれませんが、ドキュメント保守に無駄な工数を割り当てることは本来は不要なことです。  
またエクセルやワードは、コードからの自動生成やAIによる修正も困難です。  
ドキュメント保守については特に重要であるため、詳細は別途[保守可能なドキュメント管理](articles/maintainability_document.md)章として章を立てているので、これを参照してください。

## まとめ

この章では、保守性を担保するための手法を紹介しました。  
[品質担保手法](articles/quality_assurance_techniques.md)章で紹介した手法も合わせて適用することで、システム開発の要件定義から運用・保守までをスムーズに進めることが可能です。  
