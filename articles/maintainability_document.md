# 保守可能なドキュメント管理

ドキュメントは、保守可能でなければなりません。  
もしドキュメントの修正の差分の履歴が管理できておらず、修正者が誰かもわからず、修正が関係者各位へ配布されないのであれば、それは保守ができているとはとても言えません。

この章では、運用・保守を見据えたドキュメント管理について説明します。

## 保守可能なドキュメント管理とは？

一度作ったら終わりのドキュメントと違って保守可能なドキュメント管理とは、以下のような機能を備えていなければなりません。

- 編集した日時や編集箇所、編集者の管理ができること
- テキストや図の編集した内容、差分や編集履歴の確認が容易であること
- 編集・修正タスクのタスク管理が可能であること
- プログラムやスキーマ定義から自動生成できること
- 関係者へ速やかに配布がされること
- どんな環境からもスムーズな閲覧ができること

しばしばドキュメント内に編集履歴ページを作り手動で履歴を管理しているドキュメントもありますが、それだとこっそり（しばしば悪意で以って）修正が可能であり、そうでなくてもケアレスミスにより履歴の追加漏れも発生するため保守可能であるとはとても言えません。  

本ドキュメントは、Gitで管理しており、Markdownで書かれています。図はPlantUMLで書かれています。PDFやHTMLの生成も自動化されています。  
テキストベースで書かれているためGitなどで管理することが容易で、矢印の方向一つさえ管理の目から逃れることはありません。  
履歴は全て管理されていて、誰もこっそり修正したりはできません。  
自動生成されたHTMLは誰もがどんな環境でも常に最新の状態で閲覧することができ、自動生成されたPDFは常に最新版を関係者へ自動で配信可能です。  

## Markdown

Markdownはテキストベースの軽量マークアップ言語です。テキストベースで見出しや表、リストなどのドキュメント作成に必要な機能が備わっています。  
本ドキュメントもMarkdownで書かれています。  
[保守性担保手法](articles/maintainability_assurance_techniques.md)の章で使用しているクラス図やAWSアーキテクチャ図などはPlantUMLで書かれていますが、MarkdownやPlantUMLも含めて全てテキストベースで書かれています。  
エクセルやワード等と違ってテキストベースで書かれているためGitなどで管理することが容易で、矢印の方向一つさえ管理の目から逃れることはありません。  
また、MarkdownはWikipediaなどのWikiを書くためにも使われているためネット上に資料が十分にあり、編集するのに困る人はいないでしょう。  

## PlantUML

PlantUMLは図を表現するための形式です。  
PlantUMLもMarkdownと同様にネット上に資料が十分にあり、これも編集するのに困る人はいないでしょう。

例として、以下のようなPlantUMLを書いたとします。

````markdown
@startuml

interface Iterator<Item> {
  -type Item
  +next(&mut self) -> Option<Self::Item>
}

note top of Iterator
  <b>■Iteratorトレイト</b>
  イテレータを実装するためのトレイト。
  先頭から順次走査可能な
  オブジェクトであることを示す。
end note

struct Vec <Item> {
  -buf: RawVec<Item>
  -len: usize
  +count(self) -> usize
  +nth(&mut self, n: usize) -> Option<Self::Item>
  +len(&self) -> usize
  +next(&mut self) -> Option<Self::Item>
}

note bottom of Vec 
  <b>■Vec構造体</b>
  サイズを変更可能な配列。
  そのサイズはコンパイル時には不定で、
  （配列のサイズが可変なのは他言語では普通だが
  Rustでは安全性のため配列の要素数は不変である）
  いつでも要素の追加・削除が可能。
  内部では配列で値を保持している。
end note

struct LinkedList <Item> {
  -head: Option<NonNull<Node<Item>>>
  -tail: Option<NonNull<Node<Item>>>
  -len: usize
  -alloc: Allocator
  +next(&mut self) -> Option<Self::Item>
}

note bottom of LinkedList
  <b>■LinkedList構造</b>
  Vecと同様にサイズを変更可能な配列。
  Vecは配列で実装されているが
  LinkedListは連結リストで実装されている。
end note

Iterator <|-- Vec
Iterator <|-- LinkedList

@enduml
````

これは、以下のような図になります。

```plantuml
@startuml

interface Iterator<Item> {
  -type Item
  +next(&mut self) -> Option<Self::Item>
}

note top of Iterator
  <b>■Iteratorトレイト</b>
  イテレータを実装するためのトレイト。
  先頭から順次走査可能な
  オブジェクトであることを示す。
end note

struct Vec <Item> {
  -buf: RawVec<Item>
  -len: usize
  +count(self) -> usize
  +nth(&mut self, n: usize) -> Option<Self::Item>
  +len(&self) -> usize
  +next(&mut self) -> Option<Self::Item>
}

note bottom of Vec 
  <b>■Vec構造体</b>
  サイズを変更可能な配列。
  そのサイズはコンパイル時には不定で、
  （配列のサイズが可変なのは他言語では普通だが
  Rustでは安全性のため配列の要素数は不変である）
  いつでも要素の追加・削除が可能。
  内部では配列で値を保持している。
end note

struct LinkedList <Item> {
  -head: Option<NonNull<Node<Item>>>
  -tail: Option<NonNull<Node<Item>>>
  -len: usize
  -alloc: Allocator
  +next(&mut self) -> Option<Self::Item>
}

note bottom of LinkedList
  <b>■LinkedList構造</b>
  Vecと同様にサイズを変更可能な配列。
  Vecは配列で実装されているが
  LinkedListは連結リストで実装されている。
end note

Iterator <|-- Vec
Iterator <|-- LinkedList

@enduml
```

AWSアーキテクチャ図はPlantUMLで以下のように書くことができます。

````
@startuml

' https://www.plantuml.com/plantuml/uml/

skinparam shadowing false
skinparam linetype ortho
skinparam rectangle {
    BackgroundColor AWS_BG_COLOR
    BorderColor transparent
}

title AWSを活用したサンプルシステム
top to bottom direction

!include <logos/github-icon>
!include <logos/github-actions>
!include <logos/branch-icon>
!include <awslib/AWSCommon.puml>
!include <awslib/General/Documents.puml>
!include <awslib/General/Users.puml>
!include <awslib/Groups/Generic.puml>
!include <awslib/SecurityIdentityCompliance/Cognito.puml>
!include <awslib/Storage/SimpleStorageService.puml>
!include <awslib/NetworkingContentDelivery/CloudFront.puml>
!include <awslib/General/Client.puml>
!include <awslib/GroupIcons/Cloudalt.puml>
!include <awslib/ApplicationIntegration/APIGateway.puml>
!include <awslib/Compute/Lambda.puml>
!include <awslib/Database/DynamoDB.puml>
!include <awslib/AWSSimplified.puml>


Users(clientUser, "ユーザー", User client device)

Cloudalt(aws, "AWS Cloud", AWS) {
  CloudFront(cloudFront, "CloudFront\nCDNとして使用", "CloudFront")
  SimpleStorageService(simpleStorageService, "S3\n静的ファイルを格納", "S3")
  Cognito(cognito, "Cognito\n認証に使用", "Cognito")
  APIGateway(apiGateway, "API Gateway\n認証の検証などに使用", "API Gateway")
  Lambda(lambda, "Lambda\nデータ処理に使用", "Lambda")
  DynamoDB(dynamodb, "DynamoDB\nデータ保存に使用", "DynamoDB")
  GenericGroup(components5,バッチ処理) #Transparent {
    Lambda(lambda2, "Lambda\n登録されたデータを元にファイルを生成", "Lambda")
  }
}

clientUser 0-d-> cloudFront: ファイルの要求
cloudFront 0-d-> simpleStorageService: ファイルの要求
apiGateway 0-d-> lambda: 処理実行
lambda 0-d-> dynamodb: データ登録
cloudFront -[hidden]r- cognito
clientUser 0-d-> cognito: 認証
apiGateway -l-> cognito: 認証取得
clientUser 0-d-> apiGateway: リクエスト送信\n（認証付き）

dynamodb -l-> lambda2: "データを取得"
lambda2 -u-> simpleStorageService: "ファイルを生成"

@enduml
````

これは、以下のような図になります。

```plantuml
@startuml

skinparam shadowing false
skinparam linetype ortho
skinparam rectangle {
    BackgroundColor AWS_BG_COLOR
    BorderColor transparent
}

title AWSを活用したサンプルシステム
top to bottom direction

!include <logos/github-icon>
!include <logos/github-actions>
!include <logos/branch-icon>
!include <awslib/AWSCommon.puml>
!include <awslib/General/Documents.puml>
!include <awslib/General/Users.puml>
!include <awslib/Groups/Generic.puml>
!include <awslib/SecurityIdentityCompliance/Cognito.puml>
!include <awslib/Storage/SimpleStorageService.puml>
!include <awslib/NetworkingContentDelivery/CloudFront.puml>
!include <awslib/General/Client.puml>
!include <awslib/GroupIcons/Cloudalt.puml>
!include <awslib/ApplicationIntegration/APIGateway.puml>
!include <awslib/Compute/Lambda.puml>
!include <awslib/Database/DynamoDB.puml>
!include <awslib/AWSSimplified.puml>


Users(clientUser, "ユーザー", User client device)

Cloudalt(aws, "AWS Cloud", AWS) {
  CloudFront(cloudFront, "CloudFront\nCDNとして使用", "CloudFront")
  SimpleStorageService(simpleStorageService, "S3\n静的ファイルを格納", "S3")
  Cognito(cognito, "Cognito\n認証に使用", "Cognito")
  APIGateway(apiGateway, "API Gateway\n認証の検証などに使用", "API Gateway")
  Lambda(lambda, "Lambda\nデータ処理に使用", "Lambda")
  DynamoDB(dynamodb, "DynamoDB\nデータ保存に使用", "DynamoDB")
  GenericGroup(components5,バッチ処理) #Transparent {
    Lambda(lambda2, "Lambda\n登録されたデータを元にファイルを生成", "Lambda")
  }
}

clientUser 0-d-> cloudFront: ファイルの要求
cloudFront 0-d-> simpleStorageService: ファイルの要求
apiGateway 0-d-> lambda: 処理実行
lambda 0-d-> dynamodb: データ登録
cloudFront -[hidden]r- cognito
clientUser 0-d-> cognito: 認証
apiGateway -l-> cognito: 認証取得
clientUser 0-d-> apiGateway: リクエスト送信\n（認証付き）

dynamodb -l-> lambda2: "データを取得"
lambda2 -u-> simpleStorageService: "ファイルを生成"

@enduml
```

このように、テキストベースで図を定義することで、管理が容易になるメリットがあります。

### 他のUML図

PlantUMLで書ける他のUML図の例も紹介しておきます。

### シーケンス図

````markdown
@startuml
participant User

User -> A: DoWork
activate A #FFBBBB

A -> A: Internal call
activate A #DarkSalmon

A -> B: << createRequest >>
activate B

B --> A: RequestCreated
deactivate B
deactivate A
A -> User: Done
deactivate A

@enduml
````

```plantuml
@startuml
participant User

User -> A: DoWork
activate A #FFBBBB

A -> A: Internal call
activate A #DarkSalmon

A -> B: << createRequest >>
activate B

B --> A: RequestCreated
deactivate B
deactivate A
A -> User: Done
deactivate A

@enduml
```

### ユースケース図

````markdown
@startuml
User -> (Start)
User --> (Use the application) : A small label

:Main Admin: ---> (Use the application) : This is\nyet another\nlabel

@enduml
````

```plantuml
@startuml
User -> (Start)
User --> (Use the application) : A small label

:Main Admin: ---> (Use the application) : This is\nyet another\nlabel

@enduml
```

### アクティビティ図

````markdown
@startuml
start
repeat
  :Test something;
    if (Something went wrong?) then (no)
      #palegreen:OK;
      break
    endif
    ->NOK;
    :Alert "Error with long text";
repeat while (Something went wrong with long text?) is (yes) not (no)
->//merged step//;
:Alert "Success";
stop
@enduml
````

```plantuml
@startuml
start
repeat
  :Test something;
    if (Something went wrong?) then (no)
      #palegreen:OK;
      break
    endif
    ->NOK;
    :Alert "Error with long text";
repeat while (Something went wrong with long text?) is (yes) not (no)
->//merged step//;
:Alert "Success";
stop
@enduml
```

### コンポーネント図

````markdown
@startuml

package "Some Group" {
  HTTP - [First Component]
  [Another Component]
}

node "Other Groups" {
  FTP - [Second Component]
  [First Component] --> FTP
}

cloud {
  [Example 1]
}


database "MySql" {
  folder "This is my folder" {
    [Folder 3]
  }
  frame "Foo" {
    [Frame 4]
  }
}


[Another Component] --> [Example 1]
[Example 1] --> [Folder 3]
[Folder 3] --> [Frame 4]

@enduml
````

```plantuml
@startuml

package "Some Group" {
  HTTP - [First Component]
  [Another Component]
}

node "Other Groups" {
  FTP - [Second Component]
  [First Component] --> FTP
}

cloud {
  [Example 1]
}


database "MySql" {
  folder "This is my folder" {
    [Folder 3]
  }
  frame "Foo" {
    [Frame 4]
  }
}


[Another Component] --> [Example 1]
[Example 1] --> [Folder 3]
[Folder 3] --> [Frame 4]

@enduml
```

### マインドマップ図

````markdown
@startmindmap
+ root node
++ some first level node
+++_ second level node
+++_ another second level node
+++_ foo
+++_ bar
+++_ foobar
++_ another first level node
-- some first right level node
--_ another first right level node
@endmindmap
````

```plantuml
@startmindmap
+ root node
++ some first level node
+++_ second level node
+++_ another second level node
+++_ foo
+++_ bar
+++_ foobar
++_ another first level node
-- some first right level node
--_ another first right level node
@endmindmap
```

### 状態図

````markdown
@startuml
scale 600 width

[*] -> State1
State1 --> State2 : Succeeded
State1 --> [*] : Aborted
State2 --> State3 : Succeeded
State2 --> [*] : Aborted
state State3 {
  state "Accumulate Enough Data\nLong State Name" as long1
  long1 : Just a test
  [*] --> long1
  long1 --> long1 : New Data
  long1 --> ProcessData : Enough Data
}
State3 --> State3 : Failed
State3 --> [*] : Succeeded / Save Result
State3 --> [*] : Aborted

@enduml
````

```plantuml
@startuml
scale 600 width

[*] -> State1
State1 --> State2 : Succeeded
State1 --> [*] : Aborted
State2 --> State3 : Succeeded
State2 --> [*] : Aborted
state State3 {
  state "Accumulate Enough Data\nLong State Name" as long1
  long1 : Just a test
  [*] --> long1
  long1 --> long1 : New Data
  long1 --> ProcessData : Enough Data
}
State3 --> State3 : Failed
State3 --> [*] : Succeeded / Save Result
State3 --> [*] : Aborted

@enduml
```

### オブジェクト図

````markdown
@startuml PERT
left to right direction
' Horizontal lines: -->, <--, <-->
' Vertical lines: ->, <-, <->
title PERT: Project Name

map Kick.Off {
}
map task.1 {
    Start => End
}
map task.2 {
    Start => End
}
map task.3 {
    Start => End
}
map task.4 {
    Start => End
}
map task.5 {
    Start => End
}
Kick.Off --> task.1 : Label 1
Kick.Off --> task.2 : Label 2
Kick.Off --> task.3 : Label 3
task.1 --> task.4
task.2 --> task.4
task.3 --> task.4
task.4 --> task.5 : Label 4
@enduml
````

```plantuml
@startuml PERT
left to right direction
' Horizontal lines: -->, <--, <-->
' Vertical lines: ->, <-, <->
title PERT: Project Name

map Kick.Off {
}
map task.1 {
    Start => End
}
map task.2 {
    Start => End
}
map task.3 {
    Start => End
}
map task.4 {
    Start => End
}
map task.5 {
    Start => End
}
Kick.Off --> task.1 : Label 1
Kick.Off --> task.2 : Label 2
Kick.Off --> task.3 : Label 3
task.1 --> task.4
task.2 --> task.4
task.3 --> task.4
task.4 --> task.5 : Label 4
@enduml
```

### ネットワーク図

````markdown
@startuml
!include <office/Servers/application_server>
!include <office/Servers/database_server>

nwdiag {
  network dmz {
      address = "210.x.x.x/24"

      // set multiple addresses (using comma)
      web01 [address = "210.x.x.1, 210.x.x.20",  description = "<$application_server>\n web01"]
      web02 [address = "210.x.x.2",  description = "<$application_server>\n web02"];
  }
  network internal {
      address = "172.x.x.x/24";

      web01 [address = "172.x.x.1"];
      web02 [address = "172.x.x.2"];
      db01 [address = "172.x.x.100",  description = "<$database_server>\n db01"];
      db02 [address = "172.x.x.101",  description = "<$database_server>\n db02"];
  }
}
@enduml
````

```plantuml
@startuml
!include <office/Servers/application_server>
!include <office/Servers/database_server>

nwdiag {
  network dmz {
      address = "210.x.x.x/24"

      // set multiple addresses (using comma)
      web01 [address = "210.x.x.1, 210.x.x.20",  description = "<$application_server>\n web01"]
      web02 [address = "210.x.x.2",  description = "<$application_server>\n web02"];
  }
  network internal {
      address = "172.x.x.x/24";

      web01 [address = "172.x.x.1"];
      web02 [address = "172.x.x.2"];
      db01 [address = "172.x.x.100",  description = "<$database_server>\n db01"];
      db02 [address = "172.x.x.101",  description = "<$database_server>\n db02"];
  }
}
@enduml
```

このようにPlantUMLで図を管理をすることで、保守を容易にすることができます。

## スキーマ定義やプログラムからのドキュメントの自動生成

OpenAPIのスキーマ定義からドキュメントが自動生成可能です。  
たとえば東京都では、公共施設一覧や車椅子でも利用できるバリアフリートイレ情報など様々な情報をWEBでAPIで公開してプログラムで取得可能なようにしていますが、そのAPI仕様はOpenAPIでスキーマ定義が記述されていて、以下のURLではスキーマ定義が自動生成でドキュメント化されたものが閲覧可能になっています。

オープンデータAPI - オープンデータAPIについて - 東京都オープンデータカタログサイトホームページ  
<https://portal.data.metro.tokyo.lg.jp/opendata-api/>

また、プログラムからもドキュメントが自動生成です。  
たとえばRustの標準ライブラリのドキュメントはソースのドキュメンテーションコメントから自動生成されています。  

std - Rust  
<https://doc.rust-lang.org/std/index.html>

Javaの標準ライブラリもソースのコメントから自動生成されています。

概要 (Java SE 17 & JDK 17)  
<https://docs.oracle.com/javase/jp/17/docs/api/index.html>

このように現代的な各プログラミング言語はドキュメントを自動生成するシステムを標準搭載しています。  
これを有効活用することで、スムーズな情報連携が可能です。

## 編集タスクのタスク管理

Gitは様々なプロジェクト管理ツールと連携が可能です。  
たとえばRedmineやBacklog、Jiraなどです。また、BugzillaやMantis Bug Trackerなどのフリーで利便性の高いツールも存在します。  
また、GitHubやGitLabでは標準でIssue管理ツールが備わっており、それを利用することも可能です。  
そういったGitと密な連携が可能なツールを利用して、ドキュメント編集タスクの管理が製造タスクと全く同じ要領で管理が可能となります。  

## 関係者とのドキュメント連携

本ドキュメントのように、ドキュメントをHTMLやPDFに変換して常に連携し、関係者が常に最新のドキュメントを参照および編集できるようにすることは、開発・保守・運用を維持継続する上での潤滑油となります。  
本ドキュメントはネット上に公開されていますが、ドキュメントを公開しない場合でもGitHubではGitHub Pages、GitLabではGitLab Pagesなど関係者にのみドキュメントを公開したいという要求は一般的であるためネット上にも情報は豊富にあり、これらを活用することでドキュメントの連携が普段使い慣れているシステムで利用可能となります。  

## まとめ

この章では、保守可能なドキュメント管理について説明しました。  
PDFやHTMLで簡単に常に最新のドキュメントを参照できるようにし、そして編集の責任の所在を明らかにすることは、開発・運用・保守を維持継続する上で極めて重要です。
