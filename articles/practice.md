# 実践

本ドキュメントは「AIによるシステム開発ソリューション（理論編）」として、実践編は別ドキュメントで紹介するとしていますが、実践編にて実際に利用するプログラミング言語、インフラ、フロントエンド、および周辺技術について前もって概要を説明しておきます。  

- Rust
- TypeScript
- OpenAPI
- AWS・CloudFormation
- GitHub Copilot
- GitHub Actions

## Rustについて

Rustとは、安全性・性能・実績、いずれの面でも優れているプログラム言語です。  
まずはRustについて紹介します。

### Rustの安全性

ご存知の通り我々の使用しているAndroidは日々セキュリティ上の脆弱性が発見され頻繁に修正アップデートが加わっています。しかし近年、Androidの新規プログラムがRustで書かれ始めて以降、Rustで書かれた部分に重大なセキュリティ上の脆弱性が発見されたことはありません。  

Rustではプログラミング・コンパイルの時点で様々なエラーを検出してくれます。Rustで実装したプログラムでは以下の安全性が保証されています。  

- **型安全**  
  Rustでは型が厳格に取り扱われています。RubyやPython、PHPなどではテスト時や本番時にしか判明しないエラーが、Rustではプログラミング・コンパイル時にエラーとして取り扱われます。
- **NULL安全**  
  Javaでしばしば「NullPointerException（通称ぬるぽ）」が問題になることがありますが、RustではNULLが適切に取り扱われるためNULLに関する問題が一切発生しません。
- **メモリ安全**  
  Rustではメモリが厳格に取り扱われています。メモリの解放漏れもありませんし、(`unsafe`を使用しない通常の)Rustのプログラムではメモリリークも発生しません。
- **スレッド安全**  
  並列処理時を実行する場合、他の言語では値の更新の競合や意図しない値の上書きなどが発生してしまいがちですが、Rustではそのようなことは発生しません。

Rustでは（`unsafe`などを使用することで意図的に安全でないプログラムを書くことはできますが）普通にプログラムを書けば、自動的に安全なプログラムを書くことができるのです。  
例えば、自動車の組み込みシステムなどの安全性を担保するための「MISRA C」というC言語の規格がありますが、Rustでは普通にプログラムを書くだけでこの「MISRA C」の規格の8～9割を満たすと言われています。  

### Rustの性能

Rustで書かれたプログラムはCおよびC++で書かれたプログラムに匹敵するほど高速に動作します。つまり、ほぼ全てのプログラミング言語で書かれたプログラムよりも高速に動作することを意味します。  
また、Javaなどと違ってGC(ガベージコレクション)言語ではないため、メモリ管理で処理時間を取られることもなく、GCが無い分だけ実装のフットプリントも小さいです。  
当然、JavaScriptやRuby、Python、PHP、Javaなどよりも高速です。また使用されるメモリ量もこれらの言語より少なく、実装も軽量です。

### Rustの実績

以上のように、現代の最も優れたプログラミング言語であるRustは、ありとあらゆる場所で使われています。  
実績のごく一部を紹介します。

- **Mozilla Firefox**  
  現在、Rustを使って書き直されています。
- **Android**  
  Android12でサポートを開始して、Android13からは、新しいコードの大半がRustで書かれています。
- **Windows**  
  Windows 10から一部の重要なプログラムをRustに置き換えています。
- **Linux**  
  バージョン6.1から実装をRustに置き換えています。
- **AWS Lambda**  
  AWS Lambdaを実装しているFirecrackerではRustが使用されています。
- **AWS S3**  
  AWS S3を実装しているShardStoreではRustが使用されています。
- **Discord**  
  基盤実装のRead StatesをRustで刷新しました。
- **Dropbox**  
  Dropboxを実装しているエンジンであるNucleusをRustで刷新しました。


以上のように、Rustは名実ともに最先端のプログラミング言語であり、新規プロジェクトでRust以外の言語を採用するメリットは一切ありません。  
新規システム開発でRust以外を使用することは、COBOLで新しくシステムを開発するのと同じくらいなにか特別な理由でもない場合には非合理的なことです。  
特別な理由の一つに、Rustが動かない端末での実装というのがあります。たとえばWEBシステムのフロントエンドなどです。次項ではWEBシステムのフロントエンドの実装のベストプラクティスとしてTypeScriptを紹介します。  

## TypeScriptについて

ブラウザ上ではRustは動きません。そのため、WEBシステムのフロントエンドはRust以外の言語で実装する必要があります。そこで使用するのがTypeScriptです。

ここではTypeScriptについて紹介します。

現代のWEBシステムでは一部の古くて保守性の悪いシステム以外はすべてTypeScriptで作られています。  
もちろん、TypeScript以外でもWEBシステムの開発は可能です。しかし、1度作ってあとは放ったらかしならともかくとして、保守の必要があるWEBシステムではTypeScriptで作ることが現代では当たり前です。  

TypeScriptは型安全のプログラミング言語のため、かつてはテスト時や本番時にしか判明しなかったような多くのエラーが、今ではプログラミング・コンパイル時に判明するようになっています。  
バグの混入を無くし、テストの工数を減らし、そしてセキュリティ上の脆弱性の混入を未然に防ぐことができる、保守性のあるシステムを構築するためには現代では必須のプログラミング言語、それがTypeScriptです。  

## OpenAPIについて

バックエンドから取得したデータをフロントエンドで処理する際に「この値は必ず返されるのではなかったのか？」「この値が数値ではなく文字列で返されるなんて！」「エラー時にはどこにエラーコードが入るんだ？」という悩みが発生して無駄な時間を過ごすことがあります。  
それを解決するのがスキーマ駆動開発であり、そのスキーマを定義するのがOpenAPIです。  

ここではOpenAPIについて紹介します。  

OpenAPIは、APIのスキーマを定義するための標準規格です。  
かつては独自規格としてSwaggerという名前でしたが、2017年に規格が標準化され現在ではOpenAPIという名前になっていますがネット上の日本語の情報ではSwaggerという名前で説明されている記事もまだ多いです。  

OpenAPIでスキーマを定義すると以下のようなメリットがあります。

- プログラムから読み込むことができる  
  RustやTypeScriptなどで読み込み、型情報などをスキーマ定義から読み込み、プログラムをある程度自動で生成することができます。これにより、コードの予測インテリセンスが利用できたり、エラー処理の漏れなどを防止できたり、綴りや型の誤りなどのケアレスミスを検知することができます。
- スキーマからドキュメントを自動生成することができる  
  スキーマの定義からドキュメントを自動で生成することができます。これにより、開発者同士スムーズに連携できたり、会社間のやりとりが自動化できたりします。
- AIによりスキーマからプログラムを自動生成することができる  
  AIがプログラムを自動生成する際にスキーマ情報を参照してより正確にプログラムを生成することができるようになります。
- テスト値を返すテストサーバを自動で生成することができる  
  バックエンドシステムの開発中や、テスト用の値を返すテストサーバを、OpenAPIツールにより自動で生成することができます。

## AWS・CloudFormationについて

ここではAWSと、それを構築するIaC、およびそれを管理するGitの運用について紹介します。

システムはマイクロサービスアーキテクチャでサーバレスアーキテクチャで構築しており、構築のコードの管理はGitHubで管理しており、デプロイはGitHub Actionsを利用しています。  

メリットは以下の通りです。  

- SLA（サービスレベルアグリーメント）が高い  
  以下の通り、CloudFrontやS3、Lambdaなど、各サービスがそれぞれ非常にSLAが高く、仮に全体を直列として稼働率を計算したとしても全体の稼働率は99.6%を超えます。EC2のSLA(インスタンス単位で99.5%)と比べるとその信頼性の高さがわかります。
  
  | サービス名 | SLA |
  | -- | -- |
  | Amazon S3 | 99.9% |
  | Amazon CloudFront | 99.9% |
  | AWS Lambda | 99.95% |
  | Amazon API Gateway | 99.99% |
  | Amazon Cognito | 99.9% |
  | Amazon DynamoDB | 99.999%<br>※1リージョンの場合は99.99% |
- IaCによるインフラ構築で保守性が高い  
  IaCはGitHub Actionsでデプロイも自動的に実行されるため、人の手によるケアレスミスも発生せず、履歴も残るというメリットもあります。また、コードレビューと同様にインフラ管理も有識者やAIによるレビューも可能になります。

アーキテクチャ図は以下の通りです。

```plantuml
@startuml

' https://www.plantuml.com/plantuml/uml/

skinparam shadowing false
skinparam linetype ortho
skinparam rectangle {
    BackgroundColor AWS_BG_COLOR
    BorderColor transparent
}

title AIによるシステム開発ソリューション（実践編）サンプルWEBシステム
top to bottom direction

' https://github.com/awslabs/aws-icons-for-plantuml/blob/main/AWSSymbols.md
!include <awslib/AWSCommon.puml>
!include <awslib/General/Documents.puml>
!include <awslib/General/Users.puml>
!include <awslib/Groups/Generic.puml>
!include <awslib/SecurityIdentityCompliance/Cognito.puml>
!include <awslib/Storage/SimpleStorageService.puml>
!include <awslib/NetworkingContentDelivery/CloudFront.puml>
!include <awslib/General/Client.puml>
!include <awslib/GroupIcons/Cloudalt.puml>
!include <awslib/ApplicationIntegration/APIGateway.puml>
!include <awslib/Compute/Lambda.puml>
!include <awslib/Database/DynamoDB.puml>
!include <awslib/AWSSimplified.puml>

' https://github.com/plantuml/plantuml-stdlib/tree/master
!include <logos/github-icon>
!include <logos/github-copilot>
!include <logos/github-actions>
!include <logos/branch-icon>



Users(clientUser, "ユーザー", User client device)
Users(developUser, "関係者各位", Develop User client device)

Cloudalt(aws, "AWS Cloud", AWS) {
  GenericGroup(components1,WEB) {
    CloudFront(cloudFront, "CloudFront\nCDNとして使用", "CloudFront")
    Cognito(cognito, "Cognito\n認証に使用", "Cognito")
    APIGateway(apiGateway, "API Gateway\n認証の検証などに使用", "API Gateway")
  }
  SimpleStorageService(simpleStorageService, "S3\n静的ファイルを格納", "S3")
  Lambda(lambda, "Lambda\nデータ処理に使用", "Lambda")
  DynamoDB(dynamodb, "DynamoDB\nデータ保存に使用", "DynamoDB")
  GenericGroup(components5,バッチ処理) {
    Lambda(lambda2, "Lambda\n登録されたデータを元にファイルを生成", "Lambda")
  }
}

frame "<$github-icon>\nGitHub" as gitHub {
  rectangle "$DocumentsIMG()\nドキュメント" as docs
  rectangle "$DocumentsIMG()\nコード・および\nCloudFormation定義" as branch
  rectangle "<$github-actions>\nGitHub Actions" as gitHubActions
  rectangle "<$github-copilot>\nGitHub Copilot" as gitHubCopilot
}

Client(developer, "開発者", Developer client device)


clientUser 0-d-> cloudFront: ブラウザから\nアクセス
cloudFront 0-d-> simpleStorageService: ファイルの要求
apiGateway 0-d-> lambda: 処理実行
lambda 0-d-> dynamodb: データ登録
cloudFront -[hidden]r- cognito
clientUser 0-d-> cognito: 認証
apiGateway -r-> cognito: 認証取得
clientUser 0-d-> apiGateway: リクエスト送信（認証付き）

dynamodb -l-> lambda2: "データを取得"
lambda2 -u-> simpleStorageService: "ファイルを生成"


' aws -[hidden]d- gitHub 


developer -d-> gitHubCopilot: "修正指示"
' developer -l-> branch: "コード直接修正"
gitHubCopilot -d-> branch: "修正"
gitHubCopilot -[hidden]d-> gitHubActions

' developer -u-> docs: "ドキュメント直接修正"
gitHubCopilot -> docs: "ドキュメント修正"
branch -d-> gitHubActions: "デプロイ"

gitHubActions -u-> docs: "ドキュメント\n生成・更新"
docs -l-> developUser: "ドキュメント\n連携"

gitHubActions ----> aws: "インフラ\n構築"

@enduml
```


## GitHub Copilotについて

以上で紹介した、Rust・TypeScript・OpenAPI・CloudFormationをAIに生成させるために使用するのがGitHub Copilotです。

GitHub Copilotについてはもう説明不要とは思いますが、詳細については公式ホームページを参照してください。

GitHub Copilot  
<https://github.com/features/copilot>

## GitHub Actionsについて

GitHub CopilotはVSCodeやGitHub Actions上で使用します。  
GitHub ActionsはCloud9のようなクラウド上で使用する開発環境で、開発環境を丸ごと配布することが簡単なので、実践編では実際にGitHub Actions上で即座に開発が開始できる状態から始めます。


## ドキュメント管理について

ドキュメント管理の手法については[保守可能なドキュメント管理](articles/maintainability_document.md)章で説明していますので参照してください。  

今回の実践では、ドキュメント管理はGitHubで行います。  
ドキュメント記述はMarkdownで行い、図はPlantUMLで記述し、PDFやHTMLの生成にはGitHub Actionsを使用します。  

## まとめ

ここでは簡単に説明するだけにとどめましたが、別ドキュメント「AIによるシステム開発ソリューション（実践編）」ではハンズオンとして本章で説明したこれらを実際に活用しながら、AIによりプログラミング・インフラ管理・ドキュメント管理を実際にしてみます。
