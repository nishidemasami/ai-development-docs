# AIプログラミングプロダクトの紹介

かつては、あなたが1ヶ月働いても1人月しかプログラミング作業をすることはできませんでした。  
あなたがたとえ1ヶ月24時間プログラミングをし続けても3人月のプログラミング作業しかできなかったのです。  
今までは1人でできるプログラミングには限りがあったのです。

しかし、今は違います。AIを駆使することで、大量の作業をAIに処理させることができるようになりました。  
ZOZO[^1]やDMM.com[^2]などは全社でGitHub Copilotを導入し一定の効果があったと報告しています。  
かつて大規模案件では熟練プログラマだけでは人員確保ができずに泣く泣く新人やロースキルプログラマを採用してきたようなことがありましたが、AIを活用することにより大規模案件でも少数精鋭の熟練プログラマだけで作業できるようになることもありえるのです。  

この章では、プログラミングのための様々なAIのプロダクトを紹介します。  


## AIプロダクト一覧


|  開発会社・製品名 | 料金（月額・1人あたり） |  主な対応言語 |
| ------------- | ------------- | ------------- |
| MicroSoft<br>GitHub Copilot  |  個人：10ドル <br>ビジネス：19ドル<br>エンタープライズ：39ドル | C<br>C++<br>C#<br>Go<br>Java<br>JavaScript<br>PHP<br>Python<br>Ruby<br>Scala<br>TypeScript |
| GitLab<br>GitLab Duo Pro  | 19ドル<br>※Premiumプラン以上のみ | C++<br>C#<br>Go<br>Google SQL<br>Java<br>JavaScript<br>Kotlin<br>PHP<br>Python<br>Ruby<br>Rust<br>Scala<br>Swift<br>TypeScript |
| Amazon<br>Amazon CodeWhisperer  | 個人開発者：無料 <br>プロフェッショナル：19ドル | Python<br>Java<br>JavaScript<br>TypeScript<br>C#<br>Go<br>Rust<br>PHP<br>Ruby<br>Kotlin<br>C<br>C++<br>シェルスクリプト<br>SQL<br>Scala<br>JSON<br>YAML<br>HCL |
| Google<br>Google Duet AI for Developers   | 19ドル<br>※年間契約のみ |  Bash<br>C<br>C++<br>C#<br>Dart<br>Go<br>GoogleSQL<br>Java<br>JavaScript<br>Kotlin<br>Lua<br>MatLab<br>PHP<br>Python<br>R<br>Ruby<br>Rust<br>Scala<br>SQL<br>Swift<br>TypeScript<br>YAML |

## GitHub Copilot

GitHub Copilot  
<https://github.com/features/copilot>

この中で最もネット上に情報が存在するのがGitHub Copilotです。使い方などもネット上で詳しく説明されているので、一番とっつきやすいです。  
VSCodeに拡張機能を入れるだけで使えるので参入障壁も低く、誰にでも使えます。  
GitHub Copilot ChatというAIチャットサービスにより、チャットで依頼するだけでプログラムの生成や修正を依頼したり、コードの概要を生成したり、テストを作成したりすることも可能です。  

## GitLab Duo Pro

GitLab Duo  
<https://about.gitlab.com/gitlab-duo/>

GitLab DuoはGitHub Copilotよりもより幅広くの行程をAIでカバーできるプロダクトで、プログラミングだけでなく、レビューや、プログラムの内容や修正内容の概要の生成、テストの作成など、さまざまな工程でAIを活用することができます。  
また、GitLab Duo ChatというAIチャットサービスにより、チャットで依頼するだけでプログラムを生成することも可能です。  
以前は「GitLab Duo Code Suggestions」という名前でAIプログラミングサービスを提供していましたが、AIチャットサービスやAIリファクタリングサービスなどと一緒になって「GitLab Duo Pro」というパッケージとしてPremiumプラン以上のユーザーにアドインとして提供されるようになりました。  

## Amazon CodeWhisperer

Amazon CodeWhisperer  
<https://aws.amazon.com/jp/codewhisperer/>

Amazonが提供しているAIコードジェネレーターです。  
個人で試してみる分には（利用できる機能には制限があるものの）無料で使用できるため、学習には最適です。  
Amazon QというAIチャットサービスを使用することにより、チャットで依頼するだけでプログラムの概要を生成したり、テストを生成したりすることが可能です。また、他の製品にない特徴として、脆弱性のあるプログラムがないかどうかをスキャンしてくれる「セキュリティスキャン」という機能などもあります。これらも無料版でも利用が可能です。  

## Google Duet AI for Developers

Google Duet AI for Developers  
<https://cloud.google.com/duet-ai?hl=ja>

Googleが提供しているアプリケーション開発支援AIです。  
Duet AI ChatというAIチャットサービスに質問することで、クラウドに関する質問への回答や、ベストプラクティスに関するガイダンスを受け取ることができます。  
GoogleのサービスだけあってGoogle Cloudと密に連携していて、Google CloudにデプロイするためのTerraformコードの生成をDuet AI Chatに依頼することが可能です。  

## まとめ

ネット上にはGitHub Copilotの情報が数多く存在するため、まずはGitHub Copilotから試してみるととっつきやすいと思います。

また、ここで紹介したもの以外にも、世の中には無数にそういったプロダクトが存在するので、各自案件に適合するものを探してみてください。

[^1]: https://techblog.zozo.com/entry/introducing_github_copilot
[^2]: https://www.macnica.co.jp/business/dx/manufacturers/github/case_dmm.html